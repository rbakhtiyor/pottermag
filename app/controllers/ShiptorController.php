<?php

namespace app\controllers;

use AmoCRM\Client;
use app\components\Cart;
use app\components\Shiptor;
use app\models\Order;
use app\models\OrderProduct;
use yii\base\Model;
use Yii;
use yii\web\NotFoundHttpException;

class ShiptorController extends \yii\web\Controller
{
    public function actionIndex($id = null)
    {
        if (!\Yii::$app->session->has("order_id")) {
            $order = new Order();
            $order->save();
        } else {
            $order = Order::findOne(\Yii::$app->session->get(\Yii::$app->cart->getSessionKey()));

        }

        /** @var Client $amo */
        $amo = \Yii::$app->amocrm;
        $contact = $amo->contact;
        $lead = $amo->lead;
        if ($id != null) {
            $order->amo_id = $id;

            $current_lead = $lead->apiList([
                'query'=>$id,
                'limit_rows' => 1,
            ]);

            if (isset($current_lead[0])) {

                $current_contact = $contact->apiList([
                    'id'=>$current_lead[0]['main_contact_id'],
                    'limit_rows' => 1,
                ]);

                if (isset($current_contact[0])) {
                    $order->first_name = $current_contact[0]['name'];
                    foreach ($current_contact[0]['custom_fields'] as $item) {

                        if ($item['id'] == 204967) {
                            $order->phone = $item['values'][0]['value'];
                        }

                        if ($item['id'] == 204969) {
                            $order->email = $item['values'][0]['value'];
                        }
                    }
                }


            }

        }


        $label_url = null;
        $track_code = null;


        if ($order->load(\Yii::$app->request->post()) && $order->save()) {

            $shiptor = new Shiptor();
            $s = $shiptor->addPackage($order);

            if (isset($s['result']['id'])) {

                $order->shiptor_id = $s['result']['id'];

                $order->delivery_amount = $s['result']['cost']['total_cost'];
                $label_url = $s['result']['label_url'];
                $track_code = $s['result']['tracking_number'];

            } else {    

                $order->delivery_amount = 0;
            }
            Yii::$app->session->remove("kladr");


            $order->save();

            $this->addOrderToCrm($order->id, $label_url,$track_code);

            \Yii::$app->session->remove("order_id");

        }




        return $this->render('index', [
            'order'=>$order
        ]);
    }


    public function addOrderToCrm($order_id, $track_code, $label_url)
    {
        $order = Order::findOne($order_id);
        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();
        if ($order == null) {
            throw new NotFoundHttpException();
        }
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();





        $lead = $amo->lead;
        $lead->debug(false); // Режим отладки
        $lead['name'] = "Заказ №" . $order->id;
//        $lead['status_id'] = 15501178;
        $lead['price'] = $order->getTotalPrice();
        $lead['responsible_user_id'] = 15501169;
        $lead->addCustomField(341541, $order->shiptor_id);
        $lead->addCustomField(341597, $order->address5);
        $lead->addCustomField(341721, $order->id);
        $lead->addCustomField(390931, $track_code);
        $lead->addCustomField(390945, $label_url);
        $lead->addCustomField(390945, $label_url);

        $order_products_as_text = '';
        $cost_price = 0;
        /** @var OrderProduct[] $order_products */
        foreach ($order_products as $order_product) {
            if ($order_product->product_id  == 8) {
                $cost_price += (1200 * $order_product->count);
            } else {
                $cost_price += (172 * $order_product->count);
            }
            $order_products_as_text .= strip_tags($order_product->product->title) . ' кол-во: ' . $order_product->count . "\n";
        }


        $lead->addCustomField(342247, $order_products_as_text);
        $lead->addCustomField(391261, $cost_price);




        $id = $lead->apiUpdate($order->amo_id);


        $order->save();

    }

    public function actionClear()
    {
        \Yii::$app->session->remove("order_id");
        return $this->redirect(['shiptor/index']);

    }











}
