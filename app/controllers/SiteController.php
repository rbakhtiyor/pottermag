<?php

namespace app\controllers;

use AmoCRM\Client;
use andkon\yii2kladr\Kladr;
use andkon\yii2kladr\KladrApi;
use andkon\yii2kladr\Query;
use app\components\Cart;
use app\components\Shiptor;
use app\forms\CallbackForm;
use app\forms\QuestionForm;
use app\models\Order;
use app\models\OrderProduct;
use Omnipay\Omnipay;
use yandexmoney\YandexMoney\Message\AuthorizeRequest;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\forms\LoginForm;
use app\forms\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],

        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {




        $status = Yii::$app->request->get("status", false);
        if ($status == 'success') {
            Yii::$app->session->setFlash("success_order");
            $status = false;

        }

        $callback_model = new CallbackForm();
        if ($callback_model->load(Yii::$app->request->post()) && $callback_model->send()) {
            Yii::$app->session->setFlash("success_callback");
        }

        $order = Order::findOne(Yii::$app->session->get(Yii::$app->cart->getSessionKey()));
        if ($order == null) {
            $order = new Order();
        }
        $order->payment_type = Order::PAYMENT_TYPE_CASH;



        $label_url = null;
        $track_code = null;
        if ($order->load(Yii::$app->request->post()) && $order->save()) {
            if ($order->delivery_id != 999) {
                $shiptor = new Shiptor();
                $s = $shiptor->addPackage($order);



                if (isset($s['result']['id'])) {

                    $order->shiptor_id = $s['result']['id'];

                    $order->delivery_amount = $s['result']['cost']['total_cost'];
                    $label_url = $s['result']['label_url'];
                    $track_code = $s['result']['tracking_number'];

                } else {

                    $order->delivery_amount = 0;
                }
            }

            Yii::$app->session->remove("kladr");


            $order->save();

            $this->addOrderToCrm($order->id, $label_url,$track_code);

            return $this->redirect(['site/payment', 'order_id'=>$order->id]);
        }

        $question = new QuestionForm();
        if ($question->load(Yii::$app->request->post()) && $question->send()) {


            Yii::$app->session->setFlash("success_question");
        }


        return $this->render('index');
    }

    public function actionPayment($order_id)
    {


        $order = Order::findOne($order_id);
        if ($order == null) {
            throw new NotFoundHttpException("Order not found.");
        }

        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();

        $items = [];
        /** @var OrderProduct $order_product */
        foreach ($order_products as $order_product) {
            $items[] = [
                'quantity'=>$order_product->count,
                'price'=> [
                    'amount'=>$order_product->price
                ],
//                'tax'=>3,
                'text'=>strip_tags($order_product->product->title)
            ];
        }

        $items[] = [
            'quantity'=>1,
            'price'=> [
                'amount'=>$order->delivery_amount
            ],
//            'tax'=>3,
            'text'=>strip_tags($order_product->product->title)
        ];

        $tax = [
            'customerContact'=>$order->phone,
            'taxSystem'=>1,
            'items'=>$items
        ];

        if ($order->payment_type == Order::PAYMENT_TYPE_YAMONEY) {
            $gateway = Omnipay::create('\yandexmoney\YandexMoney\Gateway');
            $gateway->setShopId(149452);
            $gateway->setScid(144262);
            $gateway->setCustomerNumber($order->id);
            $gateway->setOrderNumber($order->id);
            $gateway->setPassword("4j0iMctrMm");
            $gateway->setOrderId($order->id);
            $gateway->setMethod("PC");
            $gateway->setReturnUrl(Url::to(['site/success'], true));
            $gateway->setCancelUrl(Url::to(['site/fail'], true));
            $gateway->setReceipt($tax);





            $response = $gateway->purchase(['amount' => $order->getTotalPrice(), 'currency' => 'RUB', 'testMode' => false])->send();

            if ($response->isSuccessful()) {
                print_r($response);
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                echo $response->getMessage();
            }
        }

        if ($order->payment_type == Order::PAYMENT_TYPE_CARD) {
            $gateway = Omnipay::create('\yandexmoney\YandexMoney\Gateway');
            $gateway->setShopId(149452);
            $gateway->setScid(144262);
            $gateway->setPassword("4j0iMctrMm");
            $gateway->setCustomerNumber($order->id);
            $gateway->setOrderNumber($order->id);
            $gateway->setOrderId($order->id);
            $gateway->setMethod("AC");
            $gateway->setReturnUrl(Url::to(['site/success'], true));
            $gateway->setCancelUrl(Url::to(['site/fail'], true));

            $response = $gateway->purchase(['amount' => $order->getTotalPrice(), 'currency' => 'RUB', 'testMode' => false])->send();
            if ($response->isSuccessful()) {
                print_r($response);
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                echo $response->getMessage();
            }
        }

        if ($order->payment_type == Order::PAYMENT_TYPE_CASH) {
            return $this->redirect(['site/success']);
        }

    }

    public function actionCheck()
    {
        $order = Order::findOne(Yii::$app->request->post("orderNumber"));


        $gateway = Omnipay::create('\yandexmoney\YandexMoney\Gateway');
        $gateway->setShopId(149452);
        $gateway->setScid(144262);
        $gateway->setCustomerNumber($order->id);
        $gateway->setOrderNumber($order->id);
        $gateway->setPassword("4j0iMctrMm");
        $gateway->setOrderId($order->id);
        if ($order->payment_type == Order::PAYMENT_TYPE_CARD) {

            $gateway->setMethod("AC");
        } else {
            $gateway->setMethod("PC");
        }
        $gateway->setReturnUrl(Url::to(['site/success'], true));
        $gateway->setCancelUrl(Url::to(['site/fail'], true));

        $s = $gateway->authorize(Yii::$app->request->post());

        $response = $s->send();
        return $response->getMessage();
    }

    public function actionPayAvisio()
    {
        $order = Order::findOne(Yii::$app->request->post("orderNumber"));

        $gateway = Omnipay::create('\yandexmoney\YandexMoney\Gateway');
        $gateway->setShopId(149452);
        $gateway->setScid(144262);
        $gateway->setCustomerNumber($order->id);
        $gateway->setOrderNumber($order->id);
        $gateway->setPassword("4j0iMctrMm");
        $gateway->setOrderId($order->id);
        if ($order->payment_type == Order::PAYMENT_TYPE_CARD) {

            $gateway->setMethod("AC");
        } else {
            $gateway->setMethod("PC");
        }
        $gateway->setReturnUrl(Url::to(['site/success'], true));
        $gateway->setCancelUrl(Url::to(['site/fail'], true));

        $s = $gateway->completePurchase(Yii::$app->request->post());

        $response = $s->send();
        if ($response->getCode() == 0) {
            $order->status = Order::STATUS_SUCCESS_PAY;
            $order->save();
            $this->orderSuccessPay($order->id);
        }
        return $response->getMessage();
    }


    public function actionAddToCart($product_id)
    {
        /** @var Cart $cart */
        $cart = Yii::$app->cart;
        return $cart->add($product_id, 1);
    }

    public function actionGetOrder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Order::findOne($id);
    }

    public function actionDeleteFromCart($product_id)
    {
        /** @var Cart $cart */
        $cart = Yii::$app->cart;
        return $cart->delete($product_id);
    }


    public function actionChangeCount($product_id, $count)
    {
        /** @var Cart $cart */
        $cart = Yii::$app->cart;
        return $cart->setCount($product_id, $count);
    }

    public function actionAddKladr($kladr)
    {
        Yii::$app->session->set('kladr', $kladr);
        return true;
    }


    public function actionSetDeliveryPoint($kladr, $courier, $id)
    {
        Yii::$app->session->set('delivery_kladr', $kladr);
        Yii::$app->session->set('delivery_courier', $courier);
        Yii::$app->session->set('delivery_id', $id);

    }

    public function actionAgreement()
    {
        return Yii::$app->response->sendFile("media/agreement.pdf");
    }

    public function actionTest()
    {
//                Yii::$app->session->destroy();
//        $gateway = Omnipay::create('\yandexmoney\YandexMoney\Gateway');
//        $gateway->setShopId(149452);
//        $gateway->setScid(556020);
//        $gateway->setCustomerNumber(1);
//        $gateway->setOrderNumber(1);
//        $gateway->setOrderId(1);
//        $gateway->setMethod("AC");
//        $gateway->setReturnUrl(Url::to(['site/success'], true));
//        $gateway->setCancelUrl(Url::to(['site/fail'], true));
//
//        $response = $gateway->purchase(['amount' => '1.00', 'currency' => 'RUB', 'testMode' => true])->send();
//
//        var_dump($response);

//        if ($response->isSuccessful()) {
//            print_r($response);
//        } elseif ($response->isRedirect()) {
//            $response->redirect();
//        } else {
//            echo $response->getMessage();
//        }

        Yii::$app->session->removeAll();
        /** @var Client $amo */
        $amo = Yii::$app->amocrm;
        print_r($amo->account->apiCurrent());
//
//        $ship = new Shiptor();
//        $s = $ship->getDeliveryPoint(1600000100015590001, 'dpd', 5);
//        var_dump($s);
//        $s = $ship->calculateShipping(Yii::$app->session->get("kladr"));
//        Yii::$app->session->remove("kladr");
//        var_dump(Yii::$app->session->get("kladr"));
//        var_dump($s);
//        exit;
//        foreach ($s['result']['methods'] as $item) {
//            print_r($item);
//            var_dump($item['method']['name']);
//            var_dump($item['cost']['total']['sum']);
//            var_dump($item['days']);
//            exit;
//        }
//        print_r($s);
//        Yii::$app->session->destroy();

//        $order = Order::findOne(15);
//        $s = $ship->addPackage($order);
//        var_dump($s);

    }

    public function addOrderToCrm($order_id, $track_code, $label_url)
    {
        $order = Order::findOne($order_id);

//        var_dump($order->attributes);

        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();
        if ($order == null) {
            throw new NotFoundHttpException();
        }
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();


        $contact = $amo->contact;

        $contact['name'] = $order->first_name . ' ' . $order->last_name;
        $contact->addCustomField(204967, [
            [$order->phone, 'WORK'],
        ]);
        $contact->addCustomField(204969, [
            [$order->email, 'WORK'],
        ]);

        if (Yii::$app->session->has("client_id")) {

            $client_id = $contact->apiUpdate(Yii::$app->session->get("client_id"));


        } else {

            $client_id = $contact->apiAdd();

        }


        $lead = $amo->lead;
        $lead->debug(false); // Режим отладки
        $lead['name'] = "Заказ №" . $order->id;
        $lead['status_id'] = 15501178;
        $lead['price'] = $order->getTotalPrice();
        $lead['responsible_user_id'] = 1642822;
        $lead->addCustomField(341541, $order->shiptor_id);
        $lead->addCustomField(341597, $order->address5);
        $lead->addCustomField(341721, $order->id);
        $lead->addCustomField(390931, $track_code);
        $lead->addCustomField(390945, $label_url);
        $lead->addCustomField(298385, isset($_COOKIE["roistat_visit"]) ? $_COOKIE["roistat_visit"] : "неизвестно");
//      


        $order_products_as_text = '';
        $cost_price = 0;
        /** @var OrderProduct[] $order_products */
        foreach ($order_products as $order_product) {
            if ($order_product->product_id  == 8) {
                $cost_price += (1200 * $order_product->count);
            } else {
                $cost_price += (172 * $order_product->count);
            }
            $order_products_as_text .= strip_tags($order_product->product->title) . ' кол-во: ' . $order_product->count . "\n";
        }


        $lead->addCustomField(342247, $order_products_as_text);
        $lead->addCustomField(391261, $cost_price);

        $tags = [];
        if ($order->delivery_id == 999) {
            $tags[] = 'Самовывоз';
        }

        if ($order->payment_type == Order::PAYMENT_TYPE_CARD) {
            $tags[] = 'Онлайн оплата';
        } else {
            $tags[] = "Наложенный платежь";
        }

        $lead['tags'] = $tags;


        if (Yii::$app->session->has("lead_id")) {

            $id = $lead->apiUpdate(Yii::$app->session->get("lead_id"));

            $order->amo_id = $id;

            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $id;
            $link['to'] = 'contacts';
            $link['to_id'] = Yii::$app->session->get('client_id');
            $link->apiLink();

            $order->save();

            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $id;
            $link['to'] = 'contacts';
            $link['to_id'] = Yii::$app->session->get("client_id");
            $link->apiLink();

        } else {

            $id = $lead->apiAdd();

            $order->amo_id = $id;

            $order->save();


            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $id;
            $link['to'] = 'contacts';
            $link['to_id'] = Yii::$app->session->get('client_id');
            $link->apiLink();

        }

        Yii::$app->session->remove('lead_id');
        Yii::$app->session->remove('client_id');


//
////        var_dump($contact->getValues());
//        var_dump($client_id);
//
//        exit;

    }


    public function actionSendToCrm1($first_name, $last_name, $phone, $email)
    {
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();

        $order = Order::findOne(Yii::$app->session->get(Yii::$app->cart->getSessionKey()));
        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();

        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();

        $contact = $amo->contact;

        $contact['name'] = $first_name;
        $contact->addCustomField(204967, [
            [$phone, 'WORK'],
        ]);
        $contact->addCustomField(204969, [
            [$email, 'WORK'],
        ]);

        if (Yii::$app->session->has("client_id")) {
            $client_id =  Yii::$app->session->get("client_id");
            $contact->apiUpdate($client_id);
        } else {
            $client_id = $contact->apiAdd();
            Yii::$app->session->set("client_id", $client_id);
        }

        $lead = $amo->lead;
        $lead->debug(false); // Режим отладки
        $lead['name'] = $first_name . ' ' . $last_name;
        $lead['status_id'] = 15821097;
        if ($order != null) {

            $order_products_as_text = '';
            $cost_price = 0;

            /** @var OrderProduct[] $order_products */
            foreach ($order_products as $order_product) {
                if ($order_product->product_id  == 8) {
                    $cost_price += (1200 * $order_product->count);
                } else {
                    $cost_price += (172 * $order_product->count);
                }
                $order_products_as_text .= strip_tags($order_product->product->title) . ' кол-во: ' . $order_product->count . "\n";
            }


            $lead->addCustomField(342247, $order_products_as_text);
            $lead->addCustomField(391261, $cost_price);
            $lead['price'] = $order->getTotalPrice();
        }
        $lead['responsible_user_id'] = 1642822;
        $lead->addCustomField(298385, isset($_COOKIE["roistat_visit"]) ? $_COOKIE["roistat_visit"] : "неизвестно");

        if (Yii::$app->session->has("lead_id")) {
            $lead->apiUpdate(Yii::$app->session->get("lead_id"));
        } else {

            $lead_id = $lead->apiAdd();

            Yii::$app->session->set("lead_id", $lead_id);

            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $lead_id;
            $link['to'] = 'contacts';
            $link['to_id'] = $client_id;
            $link->apiLink();


        }

        Yii::$app->session->set("first_name", $first_name);
        Yii::$app->session->set("last_name", $last_name);
        Yii::$app->session->set("email", $email);
        Yii::$app->session->set("phone", $phone);

    }



    public function actionSendToCrm2($address, $delivery_type, $delivery_amount)
    {
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();




        $lead = $amo->lead;
        $lead->addCustomField(341597, $address );
        $lead->addCustomField(341721, Yii::$app->session->get(Yii::$app->cart->getSessionKey()));
        $lead->addCustomField(341677, $delivery_amount);
        $lead->addCustomField(385579, $delivery_type);
        $lead->debug(true); // Режим отладки


        return $lead->apiUpdate(Yii::$app->session->get("lead_id"));




    }

    public function actionSendToCrm3($payment_type)
    {
        if ($payment_type ==  "online") {
            /** @var Client $amo */
            $amo = Yii::$app->amocrm->getClient();

            $lead = $amo->lead;
            $lead['tags'] = "Онлайн оплата";



            return $lead->apiUpdate(Yii::$app->session->get("lead_id"));
        } else {
            /** @var Client $amo */
            $amo = Yii::$app->amocrm->getClient();

            $lead = $amo->lead;
            $lead['tags'] = "Наложенный платёж";

            return $lead->apiUpdate(Yii::$app->session->get("lead_id"));
        }
    }

    public function actionOrderSuccessPay($order_id)
    {

        /** @var Order $order */
        $order = Order::findOne($order_id);
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();

        $lead = $amo->lead;
        $lead->debug(true); // Режим отладки
        $lead['status_id'] = 15510670;
        $lead->apiUpdate((int)$order->amo_id, 'now');

        Yii::$app->session->remove("kladr");
        Yii::$app->session->remove("lead_id");
        Yii::$app->session->remove("client_id");



    }

    public function actionSuccess()
    {

        Yii::$app->session->remove(Yii::$app->cart->getSessionKey());

        Yii::$app->session->remove("kladr");
        Yii::$app->session->remove("lead_id");
        Yii::$app->session->remove("client_id");
        return $this->redirect(['site/index', 'status'=>'success']);
    }

    public function actionFail()
    {

        Yii::$app->session->remove(Yii::$app->cart->getSessionKey());

        Yii::$app->session->remove("kladr");

        Yii::$app->session->remove("lead_id");
        Yii::$app->session->remove("client_id");
        return $this->redirect(['site/index', 'status'=>'fail']);
    }

    public function actionCheckIsSentShiptor()
    {
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();

        $list = $amo->lead->apiList([
            'status'=>15828247,
        ]);

        $data = [];

        foreach ($list as $item) {

            foreach ($item['custom_fields'] as $field) {
                if ($field['id'] == 341541) {
                    $data[$item['id']] = $field['values'][0]['value'];

                }
            }
        }


        $shiptor = new Shiptor();

        foreach ($data as $id => $shiptor_id) {
            $result = $shiptor->getPackage($shiptor_id);

            if ($result['result']['status'] == 'sent') {
                $lead = $amo->lead;
                $lead['status_id'] = 15510664;
                $lead->apiUpdate($id);
            }

            if ($result['result']['status'] == 'delivered') {
                $lead = $amo->lead;
                $lead['status_id'] = 142;
                $lead->apiUpdate($id);
            }

//            if ($result['result']['status'] == 'waiting-pickup') {
//                $lead = $amo->lead;
//                $lead['status_id'] = 15510667;
//                $lead->apiUpdate($id);
//            }


        }

    }


    public function actionCheckIsDeliveredShiptor()
    {
        /** @var Client $amo */
        $amo = Yii::$app->amocrm->getClient();

        $list = $amo->lead->apiList([
            'status'=>15510664
        ]);





        $data = [];
        foreach ($list as $item) {

            foreach ($item['custom_fields'] as $field) {
                if ($field['id'] == 341541) {
                    $data[$item['id']] = $field['values'][0]['value'];

                }

            }
        }


        $shiptor = new Shiptor();

        var_dump($data);

        foreach ($data as $id => $shiptor_id) {
            $result = $shiptor->getPackage($shiptor_id);

            if ($result['result']['status'] == 'delivered') {
                $lead = $amo->lead;
                $lead['status_id'] = 142;
                $lead->apiUpdate($id);
            }

            if ($result['result']['status'] == 'waiting-pickup') {
                $lead = $amo->lead;
                $lead['status_id'] = 15510667;
                $lead->apiUpdate($id);
            }
        }


        /////////////////////////////////////////////////////


        $list = $amo->lead->apiList([
            'status'=>15510667
        ]);

        $data = [];
        foreach ($list as $item) {

            foreach ($item['custom_fields'] as $field) {
                if ($field['id'] == 341541) {
                    $data[$item['id']] = $field['values'][0]['value'];

                }

            }
        }




        $shiptor = new Shiptor();

        foreach ($data as $id => $shiptor_id) {
            $result = $shiptor->getPackage($shiptor_id);

            if ($result['result']['status'] == 'delivered') {
                $lead = $amo->lead;
                $lead['status_id'] = 142;
                $lead->apiUpdate($id);
            }

        }



    }


    public function actionFindPlace($query)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $shiptor = new Shiptor();
        $result = $shiptor->findPlace($query);
        $data = [];
//        var_dump($result);
        if ( is_array( $result  ) ) {

            foreach ($result['result'] as $item) {
                $parent = $item['readable_parents'];
                if ($parent == "") {
                    $parent = $item['country']['name'];
                }
                $data[] = [
                    'id' => $item['kladr_id'],
                    'title' => $item['short_readable'] . ', ' . $parent
                ];
            }

        }
        return $data;

    }

    public function actionSetKladr($kladr_id)
    {
        Yii::$app->session->set("kladr_temp", $kladr_id);
        return $kladr_id;
    }

    public function actionKladrFind($name, $parent_id, $type = 'street')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kladr = KladrApi::getInstanse();
        $query = new Query();
        if ($type == 'street') {
            $query->cityId = $parent_id;
        }

        if ($type == 'building') {
            $query->streetId = $parent_id;
        }

        $query->contentType = $type;
        $query->limit = 30;
        $query->contentName = $name;
        $s = $kladr->queryToArray($query);

        if (count($s) > 0) {
            foreach ($s as $index => $item) {
                $s[$index]['full_name'] = $item['typeShort'] . '. ' . $item['name'];
            }
            return $s;
        } else {
            return [];
        }



    }




}
