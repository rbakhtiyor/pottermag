<?php

namespace app\forms;

use AmoCRM\Client;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class QuestionForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $content;
    public $fast_order = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'required'],
            ['email', 'email'],
            [['fast_order'], 'boolean'],
            ['content', 'string']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'email' => 'Ваша почта',
            'phone' => 'Ваш телефон',
            'content' => 'Сообщение',
        ];
    }

    public function send()
    {
        if ($this->validate()) {
            try {
                /** @var Client $amo */
                $amo = Yii::$app->amocrm->getClient();


                $contact = $amo->contact;

                $contact['name'] = $this->name;
                $contact->addCustomField(204967, [
                    [$this->phone, 'WORK'],
                ]);
                $contact->addCustomField(204969, [
                    [$this->email, 'WORK'],
                ]);

                $client_id = $contact->apiAdd();

                $lead = $amo->lead;
                $lead->debug(false); // Режим отладки
                $lead['name'] = $this->name;
                $lead['status_id'] = 15501175;
                $lead['responsible_user_id'] = 15501169;
                if ($this->fast_order == true) {
                    $lead['tags'] = "Быстрый заказ";

                }
                $lead->addCustomField(296105, $this->content);


                $id = $lead->apiAdd();



                $link = $amo->links;
                $link['from'] = 'leads';
                $link['from_id'] = $id;
                $link['to'] = 'contacts';
                $link['to_id'] = $client_id;
                $link->apiLink();



            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                return false;
            }
            return true;

        }

        return false;
    }


}
