<?php
namespace app\forms;

use AmoCRM\Client;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallbackForm extends Model
{
    public $phone_number;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone_number'], 'required'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'phone_number' => 'Номер телефона',
        ];
    }

    public function send()
    {
        if ($this->validate())
        {
            /** @var Client $amo */
            $amo = Yii::$app->amocrm;

            $contact = $amo->contact;
            $contact['name'] = $this->phone_number;
            $contact->addCustomField(204967, [
                [$this->phone_number, 'WORK'],
            ]);
            $contact_id = $contact->apiAdd();

            $lead = $amo->lead;
            $lead->debug(YII_ENV_DEV); // Режим отладки
            $lead['name'] = $this->phone_number ;
            $lead['status_id'] = 15501175;
            $lead['responsible_user_id'] = 15501169;
            $lead->addCustomField(298385, isset($_COOKIE["roistat_visit"]) ? $_COOKIE["roistat_visit"] : "неизвестно");
//     


            $id = $lead->apiAdd();

            $link = $amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $id;
            $link['to'] = 'contacts';
            $link['to_id'] = $contact_id;
            $link->apiLink();



            return true;
        }

        return false;
    }
}
