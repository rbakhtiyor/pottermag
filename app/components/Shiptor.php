<?php
namespace app\components;

use app\models\Order;
use app\models\OrderProduct;
use GuzzleHttp\Client;

/**
 * Created by OSON.PRO
 * User: bakhtiyor
 * Date: 18.07.17
 * Time: 6:31
 */

class Shiptor {
    public function calculateShipping($kladr_id, $order_id)
    {
        $length = 0;
        $width = 0;
        $height = 0;
        $weight = 0;

        $box = [
            'length' => 0,
            'width' => 0,
            'height' => 0,
            'weight' => 0,
        ];
        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order_id])->all();
        $i = 0;
        $total_length = 31;
        $total_width = 21;
        $total_height = 5.5;
        $total_weight = 0.113;

        /** @var OrderProduct $order_product */
        foreach ($order_products as $order_product)
        {
            for($d=1;$d<=$order_product->count;$d++) {
                $i++;
                if ( ($i-1) % 2 == 0 ) {
                    $total_height += 5.5;
                    $total_weight += 0.113;


                }
                if ($order_product->product_id == 1) {

                    $total_weight += 0.294;
                }

                if ($order_product->product_id == 2) {

                    $total_weight += 0.336;
                }

                if ($order_product->product_id == 3) {

                    $total_weight += 0.346;
                }

                if ($order_product->product_id == 4) {

                    $total_weight += 0.430;
                }

                if ($order_product->product_id == 5) {

                    $total_weight += 0.532;
                }

                if ($order_product->product_id == 6) {

                    $total_weight += 0.442;
                }
                if ($order_product->product_id == 7) {

                    $total_weight += 0.423;
                }

                if ($order_product->product_id == 8) {
                    $total_length += 32;
                    $total_width += 22;
                    $total_height += 13;
                    $total_weight += 3.495;
                }

            }




        }



        $client = new Client([
            'headers' => [
                'x-authorization-token'=>'35aac9f2d348d852186a1fd37c631f92054fa36d'
            ]
        ]);

        $data = [
            "id"=> "JsonRpcClient.js",
            "jsonrpc"=> "2.0",
            "method"=> "calculateShipping",
            "params"=> [
                "length"=> $total_length,
                "width"=> $total_width,
                "height"=> $total_height,
                "weight"=> $total_weight,
//                "weight"=> 10,

                "kladr_id"=> $kladr_id,

            ]
        ];

//        var_dump(\GuzzleHttp\json_encode($data));exit;



        $r = $client->post( 'https://api.shiptor.ru/shipping/v1', [
            'json'=>$data
        ]);

        return \GuzzleHttp\json_decode($r->getBody()->getContents(), true);
    }


    public function addPackage(Order $order)
    {
        $client = new Client([
            'headers' => [
                'x-authorization-token'=>'35aac9f2d348d852186a1fd37c631f92054fa36d'
            ]
        ]);

        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();
        $i = 0;
        $total_length = 31;
        $total_width = 21;
        $total_height = 5.5;
        $total_weight = 0.113;

        /** @var OrderProduct $order_product */
        foreach ($order_products as $order_product)
        {
            for($d=1;$d<=$order_product->count;$d++) {
                $i++;
                if ( ($i-1 <> 0) && ($i-1) % 2 == 0 ) {
                    $total_height += 5.5;
                    $total_weight += 0.113;


                }
                if ($order_product->product_id == 1) {

                    $total_weight += 0.294;
                }

                if ($order_product->product_id == 2) {

                    $total_weight += 0.336;
                }

                if ($order_product->product_id == 3) {

                    $total_weight += 0.346;
                }

                if ($order_product->product_id == 4) {

                    $total_weight += 0.430;
                }

                if ($order_product->product_id == 5) {

                    $total_weight += 0.532;
                }

                if ($order_product->product_id == 6) {

                    $total_weight += 0.442;
                }
                if ($order_product->product_id == 7) {

                    $total_weight += 0.423;
                }

                if ($order_product->product_id == 8) {
                    $total_length += 32;
                    $total_width += 22;
                    $total_height += 13;
                    $total_weight += 3.495;
                }

            }




        }


        $params = [
            "length"=> $total_length,
            "width"=> $total_width,
            "height"=> $total_height,
            "weight"=> $total_weight,
        ];



        $street = '';
        preg_match("/ул. (.{0,15}),/", $order->address5, $res);

        if (isset($res[1])) {
            $street = $res[1];
        }

        $house = '';
        preg_match("/д. (.{0,})/", $order->address5, $res2);

        if (isset($res2[1])) {
            $house = $res2[1];
        }




        $params['departure'] = [
            'shipping_method'=> (int) $order->delivery_id,
            'address'=> [
                'country'=>'RU',
                'receiver'=>$order->first_name . ' ' . $order->last_name,
                'name'=>$order->first_name,
                'surname'=>$order->last_name,
                'email'=>$order->email,
                'phone'=>$order->phone,
                'postal_code'=>(is_int(substr($order->address5, 0, 6)) ) ? substr($order->address5, 0, 6): '',
                'street'=> $order->address2,
                'house'=> $order->address3,
                'apartment'=>$order->address4,
                'address_line_1'=>$order->address5,
                'kladr_id'=>$order->address1
            ]

        ];

        if ($order->point_id != null) {

            $params['departure']['delivery_point'] = $order->point_id;
        }

        $products = [];

        /** @var OrderProduct[] $order_products */
        $order_products = OrderProduct::find()->andWhere(['order_id'=>$order->id])->all();
        $total_price = 0;
        foreach ($order_products as $order_product) {
            $products[] = [
                'shopArticle'=>$order_product->product_id,
                'count'=>$order_product->count,
                'price'=>$order_product->price
            ];

            $total_price = $total_price + $order_product->price;
        }

        $params['declared_cost'] = 0;


        if ($order->payment_type == Order::PAYMENT_TYPE_CASH) {
            $params['cod'] = $order->getTotalPrice();
            $params['declared_cost'] = $order->getTotalPrice();
            $params['external_id'] = $order->id;
        }
//
//
        $services[] = [
            'shopArticle'=>'1',
            'count'=>1,
            'price'=>$order->delivery_amount
        ];

        $params['products'] = $products;
        $params['services'] = $services;


        $data = [
            "id" => "JsonRpcClient.js",
            "jsonrpc" => "2.0",
            "method" => "addPackage",
            "params"=>$params,
        ];


        $r = $client->post( 'https://api.shiptor.ru/shipping/v1', [
            'json'=>$data
        ]);

        return \GuzzleHttp\json_decode($r->getBody()->getContents(), true);


    }

    public function getDeliveryPoint($kladr_id,$courier, $ship_method)
    {
        $client = new Client([
            'headers' => [
                'x-authorization-token'=>'35aac9f2d348d852186a1fd37c631f92054fa36d'
            ]
        ]);

        $params = [
            'kladr_id'=>$kladr_id,
            'courier'=>$courier,
            'shipping_method'=>$ship_method
        ];




        $data = [
            "id" => "JsonRpcClient.js",
            "jsonrpc" => "2.0",
            "method" => "getDeliveryPoints",
            "params"=>$params,
        ];


        $r = $client->post( 'https://api.shiptor.ru/shipping/v1', [
            'json'=>$data
        ]);

        return \GuzzleHttp\json_decode($r->getBody()->getContents(), true);

    }


    public function getPackage($id)
    {
        $client = new Client([
            'headers' => [
                'x-authorization-token'=>'35aac9f2d348d852186a1fd37c631f92054fa36d'
            ]
        ]);

        $params = [
            'id'=>$id
        ];




        $data = [
            "id" => "JsonRpcClient.js",
            "jsonrpc" => "2.0",
            "method" => "getPackage",
            "params"=>$params,
        ];


        $r = $client->post( 'https://api.shiptor.ru/shipping/v1', [
            'json'=>$data
        ]);

        return \GuzzleHttp\json_decode($r->getBody()->getContents(), true);

    }

    public function findPlace($query)
    {
        $client = new Client([
            'headers' => [
                'x-authorization-token'=>'35aac9f2d348d852186a1fd37c631f92054fa36d'
            ]
        ]);

        $params = [
            'query'=>$query
        ];




        $data = [
            "id" => "JsonRpcClient.js",
            "jsonrpc" => "2.0",
            "method" => "suggestSettlement",
            "params"=>$params,
        ];


        $r = $client->post( 'https://api.shiptor.ru/public/v1', [
            'json'=>$data
        ]);

        return \GuzzleHttp\json_decode($r->getBody()->getContents(), true);

    }
}