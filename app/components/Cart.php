<?php
/**
 * Created by OSON.PRO
 * User: bakhtiyor
 * Date: 16.07.17
 * Time: 2:46
 */

namespace app\components;


use app\models\Order;
use app\models\OrderProduct;
use yii\base\Component;

class Cart extends Component
{
    private $session_key = 'order_id';

    /** @var Order  */
    private $_order = null;

    public function init()
    {

    }

    public function add($product_id, $count)
    {
        /** @var OrderProduct $link */
        $link = OrderProduct::find()->where(['product_id'=>$product_id, 'order_id'=>$this->getOrderId()])->one();

        if ($link == null) {
            $link = new OrderProduct();
            $link->product_id = $product_id;
            $link->order_id = $this->_order->id;
            $link->count += $count;
            $link->setPrice();
            return $link->save();
        } else {
            $link->count += $count;
            $link->setPrice();
            return $link->save();
        }
    }

    private function createOrder()
    {
        $session = \Yii::$app->session;
        $order_id = $session->get($this->session_key);
        if ($order_id != null) {
            $order = Order::findOne($order_id);
            $session->set($this->session_key, $order->id);
        } else {
            $order = new Order();
            $order->save();

            $session->set($this->session_key, $order->id);
        }

        $this->_order = $order;
    }

    public function getOrderId()
    {
        $this->createOrder();
        return $this->_order->id;
    }

    public function getOrder()
    {
        $this->createOrder();
        return $this->_order;
    }

    public function setCount($product_id, $count)
    {
        /** @var OrderProduct $link */
        $link = OrderProduct::find()->andWhere(['product_id'=>$product_id, 'order_id'=>$this->getOrderId()])->one();
        if ($link == null) {
            $link = new OrderProduct();
            $link->product_id = $product_id;
            $link->order_id = $this->getOrderId();

        }
        $link->count = $count;
        $link->setPrice();
        return $link->save();
    }

    public function delete($product_id)
    {
        /** @var OrderProduct $link */
        $link = OrderProduct::find()->andWhere(['product_id'=>$product_id, 'order_id'=>$this->getOrderId()])->one();
        if ($link == null) {
            return false;
        }
        return $link->delete();
    }

    public function setStatus($status)
    {
        $order = $this->_order;
        $order->status = $status;
        return $order->save();
    }

    public function getSessionKey()
    {
        return $this->session_key;
    }


}