<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$basePath = dirname(__DIR__);
$webRoot = dirname($basePath);
Yii::setAlias("@web", $webRoot);
$config = [
    'id' => 'basic-console',
    'basePath' => $basePath,
    'vendorPath'=>$webRoot . '/vendor',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases'=> [
        '@bower'=>'@vendor/bower-asset',
    ],
    'components' => [
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            'subdomain' => 'pottermag', // Персональный поддомен на сайте amoCRM
            'login' => 'pottermag@yandex.ru', // Логин на сайте amoCRM

            'hash' => '11d3c042bbd77663cf439cac6127d8c2', // Хеш на сайте amoCRM


            // Для хранения ID полей можно воспользоваться хелпером
            'fields' => [
//                'StatusId' => 10525225,
//                'ResponsibleUserId' => 697344,
            ],
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                'username' => '1theme@mail.ru',
                'password' => 'Dimetra_98',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    'controllerMap' => [
        'serve' => 'app\commands\ServeController'
    ],
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
