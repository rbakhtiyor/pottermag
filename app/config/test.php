<?php
$params = require(__DIR__ . '/params.php');
$dbParams = require(__DIR__ . '/test_db.php');

$basePath = dirname(__DIR__);
$webRoot = dirname($basePath);
/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => $basePath,    
    'language' => 'en-US',
    'components' => [
        'assetManager'=> [
            'basePath' => '@webroot/media/assets',
            'baseUrl' => '@web/media/assets',
        ],
        'db' => $dbParams,
        'mailer' => [
            'useFileTransport' => true,
        ],
       
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],        
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],        
    ],
    'params' => $params,
];
