<?php

/* @var $this yii\web\View */

use app\forms\LetterForm;
use app\forms\QuestionForm;
use app\models\Order;
use app\models\Product;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = 'POTTER MAG - книжний интернет-магазин';
?>
<div class="sect-1">
    <div class="e-wrapper">
        <div class="col-sm-6 col-md-5">
            <div class="row">
                <img src="/media/img/catalog-knig.png" alt="">
                <img src="/media/img/book.png" alt="" class="img-responsive book-mob">
                <ul>
                    <li>Гарри Поттер и Философский камень</li>
                    <li>Гарри Поттер и Тайная комната</li>
                    <li>Гарри Поттер и Узник Азкабана</li>
                    <li>Гарри Поттер и Кубок огня</li>
                    <li>Гарри Поттер и Орден Феникса</li>
                    <li>Гарри Поттер и Принц-полукровка</li>
                    <li>Гарри Поттер и Дары Смерти</li>
                    <li class="vertical">Полный набор</li>
                </ul>
                <div class="e-price-info">
                    <h5>5530 <span>РУБ</span></h5>
                    <h3>4590 <span>РУБ</span></h3>
                    <a href="#" class="add_to_cart" data-product-id="8">Купить набор</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-7">
            <div class="e-book-wrapper">
                <img src="/media/img/book.png" alt="" class="big-book">
            </div>
        </div>
    </div>
</div>
<div class="sect-2">
    <div class="e-wrapper">
        <div class="e-title">
            <img src="/media/img/sect-2-title.png" alt="" class="hidden-mob">
            <img src="/media/img/sect-2-title-mob.png" alt="" class="visible-mob">
        </div>
        <div role="tabpanel" id="goto">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <!-- <li role="presentation" class="active">
                     <a href="#books" aria-controls="books" role="tab" data-toggle="tab">Книги</a>
                 </li>
               <li role="presentation">
                     <a href="#sticks" aria-controls="sticks" role="tab" data-toggle="tab">Палочки</a>
                </li>-->
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="books">
                    <div class="e-sect2-catalog">
                        <?php
                        /** @var Product[] $products */
                        $products = Product::find()->andWhere(['<>', 'id', 9])->all();
                        foreach ($products as $product) {
                            ?>
                            <div class="e-sect2-item">
                                <a data-toggle="modal" href="#book<?= $product->id ?>">
                                    <h4><?= $product->title ?></h4>
                                    <div class="e-img-wrapper">
                                        <img src="/media/img/books/<?= $product->id ?>.png" alt="">
                                    </div>
                                    <div class="e-price"><?= $product->price ?><span> руб/шт</span></div>
                                </a>
                                <a href="" class="click" data-toggle="modal"
                                   data-target="#oneclick-<?= $product->id ?>-modal">Заказать в 1 клик</a>
                                <a href="#" class="add_to_cart" data-product-id="<?= $product->id ?>">добавить в
                                    корзину</a>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="sticks">...</div>
            </div>
        </div>
    </div>
</div>
<div class="invitation">
    <div class="e-wrapper">
        <div class="e-title">
            <img src="/media/img/invitation.png" alt="" class="pc-logo">
            <img src="/media/img/invitation-mob.png" alt="" class="mob-logo">
        </div>
        <div class="e-letter-left"><img src="/media/img/letter-left.png" alt=""></div>
        <div class="e-letter-right"><img src="/media/img/letter-right.png" alt=""></div>
        <?php
        $form = ActiveForm::begin([
            'id' => 'callback-form-1',

        ]);
        $letter_form = new LetterForm();
        ?>

        <?= $form->field($letter_form, 'email')->textInput([])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Получить', ['class' => 'btn-potter btn', 'name' => 'contact-button']) ?>
            <div class="clearfix"></div>
        </div>


        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
<div class="sect-3">
    <div class="e-wrapper">
        <div class="e-title"><img src="/media/img/sect3-title.png" alt=""></div>
        <div class="e-sect3-wrapper">
            <div class="e-sect3-item">
                <img src="/media/img/garry.png" alt="">
                <div class="where"><span>Бесплатная доставка</span>при онлайн оплате</div>
            </div>
            <div class="e-sect3-item">
                <img src="/media/img/delivery2.png" alt="">
                <div class="where"><span>Издательство росмэн</span>лучший перевод</div>
            </div>
            <div class="e-sect3-item">
                <img src="/media/img/delivery3.png" alt="">
                <div class="where"><span>Без предоплат</span>возможен наложенный платеж</div>
            </div>
        </div>
        <div class="e-sect3-wrapper">
            <div class="e-sect3-item">
                <img src="/media/img/delivery4.png" alt="">
                <div class="where"><span>Работаем по всей России</span>А так же в белоруси<br>и казахстане</div>
            </div>
            <div class="e-sect3-item">
                <img src="/media/img/delivery5.png" alt="">
                <div class="where"><span>Качественные книги</span>твёрдый переплёт, толстая<br>бумага, крупный шрифт
                </div>
            </div>
            <div class="e-sect3-item">
                <img src="/media/img/delivery6.png" alt="">
                <div class="where"><span>Надёжная упаковка</span>книги доедут до вас<br>в целостности</div>
            </div>
        </div>
    </div>
</div>
<div class="sect-4">
    <div class="e-wrapper">
        <div class="col-sm-8 col-md-6">
            <div class="row">
                <div class="e-title"><img src="/media/img/pay.png" alt=""></div>
                <div class="e-pay-info">Оплата наличными при получении или онлайн оплата до отправки заказа...</div>
                <img src="/media/img/pay-methods.png" alt="">
            </div>
        </div>
        <div class="col-sm-4 col-md-6">
            <div class="row">
                <div class="hat-wrapper">
                    <img src="/media/img/hat.png" alt="" class="hat">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reviews">
    <div class="e-wrapper">
        <div class="e-title"><img src="/media/img/reviews.png" alt=""></div>
        <div class="e-review-slider">
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
            <div class="e-review-slide">
                <div class="slide-wrapper">
                    <div class="review-img">
                        <img src="/media/img/review-img.png" alt="" class="person">
                        <img src="/media/img/review-vk.png" alt="" class="vk">
                    </div>
                    <div class="review-header">
                        <h5>Гарри Поттер и Филосовский Камень</h5>
                        <h4>Гарри Васильевич Поттер</h4>
                        <p>Обычный парень</p>
                    </div>
                    <div class="review-text">
                        <p>Если Вы хотите разместить в каталоге собственную игру на базе HTML5, интегрируйте Mobile SDK,
                            сверьтесь с требованиями платформы и подавайте заявку на модерацию через управление
                            приложением.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sect-5">
    <div class="e-wrapper">
        <div class="col-sm-6">
            <div class="e-castle-wrapper">
                <img src="/media/img/castle.png" alt="" class="castle">
            </div>
        </div>
        <div class="col-sm-6 right">
            <div class="row">
                <div class="e-title"><img src="/media/img/garanty.png" alt=""></div>
                <div class="e-garanty-info">
                    <ul>
                        <li>Перед отправкой книги проверяются</li>
                        <li>При самовывозе можно проверить качество перед покупкой</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sect-6">
    <div class="e-wrapper">
        <div class="e-title"><img src="/media/img/subscribe.png" alt=""></div>
        <div class="e-socail">
            <div class="sect5-col">
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?143">
                </script>

                <!-- VK Widget -->
                <div id="vk_groups"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups",
                        {mode: 2, width: "300", height: "372"}, 150240315);
                </script>
            </div>
            <div class="sect5-col">
                <iframe src="//widget.instagramm.ru/?imageW=2&imageH=2&thumbnail_size=117&type=0&typetext=pottermag&head_show=1&profile_show=1&shadow_show=1&bg=255,255,255,1&opacity=true&head_bg=46729b&subscribe_bg=ad4141&border_color=c3c3c3&head_title="
                        allowtransparency="true" frameborder="0" scrolling="no"
                        style="border:none;overflow:hidden;width:260px;height:399px;"></iframe>
            </div>
        </div>
    </div>
</div>


<div class="modal fade book-modal" id="book1">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/1_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/1_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/1_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/1_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и филосовский камень</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2006</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>400</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-1',

                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик:Гарри Поттер и Философский камень";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book2">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/2_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/2_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/2_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/2_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и тайная комната</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2006</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>480</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-2'
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и тайная комната";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book3">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/1_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/1_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/1_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/1_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри Поттер и <br>Узник Азкабана</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2006</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>484</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-3',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и Узник Азкабана";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book4">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/4_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/4_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/4_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/4_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и кубок огня</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2006</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>667</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-4',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и кубок огня";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book5">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/5_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/5_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/5_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/5_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и Орден феникса</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2006</span></li>
                                <li>Переводчик: <span>Владимир Бабков, Виктор Голышев, Леонид Мотылев</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Автор: <span>Джоан Кэтлин Роулинг</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>667</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-5',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и Орден феникса";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book6">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/6_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/6_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/6_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/6_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и Принц-полукровка</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2005</span></li>

                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>672</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-6',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и Принц-полукровка";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book7">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/7_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/7_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/7_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/7_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Гарри поттер<br>и Дары смерти</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Издательство: <span>Росмэн</span></li>
                                <li>Год издания: <span>2007</span></li>
                                <li>Язык издания: <span>Русский</span></li>
                                <li>Формат издания: <span>130х200 мм (средний формат)</span></li>
                                <li>Количество страниц: <span>640</span></li>
                                <li>Переплёт: <span>Твёрдый</span></li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>
                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-7',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Гарри Поттер и Дары смерти";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade book-modal" id="book8">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png"
                                                                                             alt=""></button>
            <div class="modal-body">
                <div class="e-modal-book-wrapper">
                    <div class="e-left">
                        <div class="e-book-slider">
                            <div class="e-book-slide"><img src="/media/img/books/8_1.jpg" alt=""></div>
                            <div class="e-book-slide"><img src="/media/img/books/8_2.jpg" alt=""></div>
                        </div>
                        <div class="e-book-prev">
                            <div class="e-preview"><img src="/media/img/books/8_1.jpg" alt=""></div>
                            <div class="e-preview"><img src="/media/img/books/8_2.jpg" alt=""></div>

                        </div>
                    </div>
                    <div class="e-right">
                        <h3>Комплет из 7 книг</h3>
                        <div class="e-shadow-box">
                            <ul>

                                <li>Гарри Поттер и Философский камень</li>
                                <li>Гарри Поттер и Тайная комната</li>
                                <li>Гарри Поттер и Узник Азкабана</li>
                                <li>Гарри Поттер и Кубок огня</li>
                                <li>Гарри Поттер и Орден Феникса</li>
                                <li>Гарри Поттер и Принц-полукровка</li>
                                <li>Гарри Поттер и Дары смерти</li>
                            </ul>
                        </div>
                        <h4>Чтобы купить<br>заполните форму</h4>

                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'callback-form-8',
                        ]);
                        $fast_order = new QuestionForm();
                        $fast_order->fast_order = true;
                        $fast_order->content = "Заказ в 1 клик: Комплет из 7 книг";
                        ?>


                        <?= $form->field($fast_order, 'name')->textInput([]) ?>
                        <?= $form->field($fast_order, 'email')->textInput([]) ?>
                        <?= $form->field($fast_order, 'phone')->textInput([]) ?>
                        <?= Html::activeHiddenInput($fast_order, 'fast_order') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                            <div class="clearfix"></div>
                        </div>


                        <?php
                        ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

