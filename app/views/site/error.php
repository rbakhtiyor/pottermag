<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error col-md-6 col-md-offset-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        — Палочка!.. Палочка сломалась! <br>
        — Скажи спасибо, что не шея.
    </p>


</div>
<div class="clearfix"></div>
