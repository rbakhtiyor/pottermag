<?php
/* @var $this yii\web\View */
/* @var $order Order */
use andkon\yii2kladr\Kladr;
use app\components\Shiptor;
use app\models\Order;
use app\models\OrderProduct;
use app\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

?>
<br>
<h3 class="text-center">Добавить заказ</h3>
<div class="col-md-10 col-md-offset-1">
    <a href="<?= Url::to(['shiptor/clear']) ?>" class="btn btn-success">Очистить заказ</a>

    <?php
    $form = ActiveForm::begin(['id'=>'order-form']);
    $order->setScenario("update");
    ?>
    <?php
    Pjax::begin(['id'=>'person', 'timeout'=>10000]);
    ?>
    <?=$form->field($order, 'amo_id')?>
    <?=Html::a("Загрузить данные", ['shiptor/index', 'id'=>Yii::$app->request->get("id")], ['class'=>'btn btn-success','id'=>'load-from-crm'])?>
    <?=$form->field($order, 'first_name')?>
    <?=$form->field($order, 'last_name')?>
    <?=$form->field($order, 'email')?>
    <?=$form->field($order, 'phone')?>

    <?php Pjax::end() ?>

    <?= $form->field($order, 'address1')->widget(Kladr::className(), [
        'type' => Kladr::TYPE_CITY,

        'options' => [
            'class' => 'form-control'
        ]
    ])
    ?>

    <?= $form->field($order, 'address2')->widget(Kladr::className(), [
        'type' => Kladr::TYPE_STREET,
        'options' => [
            'class' => 'form-control'
        ]
    ])
    ?>

    <?= $form->field($order, 'address3')->widget(Kladr::className(), [
        'type' => Kladr::TYPE_BUILDING,
        'options' => [
            'class' => 'form-control'
        ]
    ])
    ?>

    <?= $form->field($order, 'address4')->textInput(['autofocus' => true]) ?>
    <?= $form->field($order, 'address5')->hiddenInput()->label(false) ?>

    <div class="well">
        <span id="order-address5_2"><?=$order->address5?></span>
    </div>
    <?php
    Pjax::begin(['id' => 'shiptor', 'timeout' => 7000]);
    $shiptor = new Shiptor();
    $delivery = [];
    $delivery_prices = [];
    if (Yii::$app->session->has("kladr")) {

        $result = $shiptor->calculateShipping(Yii::$app->session->get("kladr"), $order->id);
        if (isset($result['result']['methods'])) {
            ?>
            <div class="form-group field-order-delivery_id">
                <label class="control-label">Тип доставки</label>
                <input type="hidden" name="Order[delivery_id]" value="20">
                <div id="order-delivery_id">
                    <?php
                    foreach ($result['result']['methods'] as $method) {
                        if ($method['method']['category'] == 'delivery-point' ) {
                            continue;
                        }

                        $delivery_prices[$method['method']['id']] = $method['cost']['total']['sum'];
                        $delivery[$method['method']['id']] = $method['method']['name'] . '(' . $method['cost']['total']['sum'] . '₽) - ' . $method['days'];

                        ?>

                        <div class="radio">
                            <label>
                                <?php
                                if ($method['method']['category'] == 'delivery-point') {


                                    ?>
                                    <input type="radio" data-id="<?=$method['method']['id']?>"
                                           data-courier="<?=$method['method']['courier']?>"
                                           class=" delivery-point" name="Order[delivery_id]"
                                           value="<?= $method['method']['id'] ?>">

                                    <?= $method['method']['name'] . '(' . $method['cost']['total']['sum'] . '₽) - ' . $method['days'] ?>

                                    <?php
                                } else {
                                    ?>
                                    <input type="radio" data-name="<?=$method['method']['name']?>"
                                           class="delivery" name="Order[delivery_id]"
                                           value="<?= $method['method']['id'] ?>">

                                    <?= $method['method']['name'] . '(' . $method['cost']['total']['sum'] . '₽) - ' . $method['days'] ?>
                                <?php } ?>

                            </label>
                        </div>


                        <?php
                    }
                    ?>


                </div>

            </div>
            <?php

        }
    }
    echo "<script>";
    echo "var delivery_price = " . json_encode($delivery_prices) . ';';
    echo "</script>";


    echo $form->field($order, 'delivery_amount')->hiddenInput()->label(false);

    Pjax::end();

    ?>

    <div class="form-group field-order-payment_type has-success">
        <label class="control-label">Тип оплаты</label>
        <input type="hidden" name="Order[payment_type]" value="">
        <div id="order-payment_type" aria-invalid="false">
            <div class="radio"><input type="radio" id="online" name="Order[payment_type]" value="3"> <label for="online">Оплата онлайн</label></div>
            <div class="radio"><input type="radio" id="cash" name="Order[payment_type]" value="2"> <label for="cash">Наложенный платеж</label></div></div>

    </div>

    <?php
    Pjax::begin(['id'=>'cart-table', 'timeout'=>10000])
    ?>
    <table class="table">
        <?php
        /** @var Product[] $products */
        $products = Product::find()->all();
        foreach ($products as $product) {
            $p = OrderProduct::find()->where(['order_id'=>$order->id, 'product_id'=>$product->id])->one();
            if ($p == null) {
                $count = 0;
            } else {
                $count = $p->count;
            }
            ?>
            <tr>
                <td><?=$product->title?></td>
                <td>
                    <div class="input-group number-spinner">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-dir="dwn">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                        <input data-product-id="<?= $product->id ?>" min="1" type="text"
                               class="qty form-control" value="<?= $count?>"/>
                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-dir="up">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                    </div>

                </td>

            </tr>
        <?php } ?>
    </table>
    <?php Pjax::end() ?>

    <?php
    echo $form->errorSummary($order);
    ?>
    <div class="text-center">
        <?=Html::submitButton('Оформить заказ', ['class'=>'btn btn-success btn-lg'])?>

    </div>


    <?php
    ActiveForm::end();
    ?>


</div>
<div class="clearfix"></div>
<br>
<br>