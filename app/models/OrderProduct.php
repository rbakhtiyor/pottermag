<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_products".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $count
 * @property integer $price
 * @property Product $product
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'price'], 'required'],
            [['order_id', 'product_id', 'count'], 'integer'],
            [['product_id'], 'exist', 'targetClass'=>Product::className(), 'targetAttribute'=>'id'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
            'price' => 'Price',
        ];
    }

    public function setPrice()
    {
        if ($this->validate(['product_id', 'count'])) {
            $product = Product::findOne($this->product_id);
            $this->price = $product->price * $this->count;
            return true;
        }

        return false;
    }


    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id'=>'product_id']);
    }

}
