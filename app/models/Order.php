<?php

namespace app\models;

use kroshilin\yakassa\CustomerInterface;
use kroshilin\yakassa\OrderInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $address4
 * @property string $address5
 * @property string $address6
 * @property integer $delivery_id
 * @property integer $shiptor_id
 * @property integer $payment_type
 * @property integer $delivery_amount
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $coupon_id
 * @property integer $point_id
 * @property integer $amo_id
 */
class Order extends \yii\db\ActiveRecord
{

    const PAYMENT_TYPE_YAMONEY = 1;
    const PAYMENT_TYPE_CASH = 2;
    const PAYMENT_TYPE_CARD = 3;

    const STATUS_PENDING = 0;
    const STATUS_SEND = 1;
    const STATUS_SUCCESS_PAY = 2;
    const STATUS_RECEIVER = 3;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'phone', 'address1', 'address2', 'payment_type', 'delivery_id'], 'required', 'on'=>'update'],
            [['amo_id', 'status', 'point_id', 'created_at', 'updated_at', 'coupon_id', 'delivery_id', 'payment_type', 'shiptor_id'], 'integer'],

            [['delivery_amount'],'number', ],
            ['email', 'email'],
            [[ 'first_name', 'last_name', 'email', 'phone', 'address1','address2','address3','address4','address5'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'email' => 'Email',
            'phone' => 'Телефон',
            'address1' => 'Город/Населенный пункт',
            'address2' => 'Улица',
            'address3' => 'Дом',
            'address4' => 'Квартира',
            'address5' => 'Полный адрес',
            'address6' => 'Выбор',
            'delivery_id' => 'Тип доставки',
            'payment_type' => 'Тип оплаты',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'coupon_id' => 'Coupon ID',
        ];
    }

    public function getId()
    {

        return $this->id;
    }

    public function getTotalPrice()
    {
        $total = 0;
        $p = OrderProduct::find()->andWhere(['order_id'=>$this->id])->all();
        foreach ($p as $item) {
            $total+=$item->price;
        }

        return $total + $this->delivery_amount;
    }



}
