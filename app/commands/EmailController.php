<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use AmoCRM\Client;
use app\components\Shiptor;
use app\models\Order;
use app\models\OrderProduct;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EmailController extends Controller
{
    public function actionSendToShiptor()
    {
        /** @var Client $amo */
        $amo = \Yii::$app->amocrm->getClient();

        $s = $amo->account->apiCurrent();


        $list = $amo->lead->apiList([
            'status'=>15510664
        ]);

        $products = OrderProduct::find()->andWhere(['order_id'=>1])->all();



//        $mailer = \Yii::$app->mailer;
//        $mailer->compose('email/order-sent', ['products'=>$products])
//            ->setFrom("1theme@mail.ru")
//            ->setTo("rbakhtiyor@gmail.com")
//            ->send();




    }
}
