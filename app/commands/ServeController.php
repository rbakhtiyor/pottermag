<?php
/**
 * Created by PhpStorm.
 * User: neron7
 * Date: 14.08.16
 * Time: 14:09
 */

namespace app\commands;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ServeController extends \yii\console\controllers\ServeController
{
    public $docroot = '@web';

}