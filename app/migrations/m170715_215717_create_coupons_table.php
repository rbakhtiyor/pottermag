<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coupons`.
 */
class m170715_215717_create_coupons_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coupons', [
            'id' => $this->primaryKey(),
            'coupons'=>$this->string()->unique(),
            'type'=>$this->smallInteger(1)->defaultValue(0),
            'price'=>$this->decimal(10,2)->defaultValue(0),
            'discount'=>$this->decimal(10,2)->defaultValue(0),
            'free_product_ids'=>$this->text()->null(),
            'params'=>$this->text()->null(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('coupons');
    }
}
