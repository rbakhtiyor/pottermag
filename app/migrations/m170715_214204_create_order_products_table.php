<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_products`.
 */
class m170715_214204_create_order_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_products', [
            'id' => $this->primaryKey(),
            'order_id'=>$this->integer()->notNull(),
            'product_id'=>$this->integer()->notNull(),
            'count'=>$this->integer()->defaultValue(0),
            'price'=>$this->decimal(10,2)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_products');
    }
}
