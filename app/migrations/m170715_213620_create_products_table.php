<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m170715_213620_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
            'description'=>$this->text()->notNull(),
            'seo_keyword'=>$this->text()->null(),
            'seo_desc'=>$this->text()->null(),
            'img'=>$this->string()->null(),
            'price'=>$this->decimal(10,2)->defaultValue(0),
            'created_at'=>$this->integer()->notNull(),
            'updated_at'=>$this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('products');
    }
}
